import models.AnnotationExample;
import annotations.CustomMethodAnnotaation;
import annotations.CustomTypeAnnotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class AnnotationTest {

    public static void main(String [] args){
        System.out.println("Processing...");

        int success = 0 ;
        int failed = 0 ;
        int total = 0 ;
        int disabled = 0;


        Class<AnnotationExample> exampleClass = AnnotationExample.class;
        if (exampleClass.isAnnotationPresent(CustomTypeAnnotation.class)){
            Annotation annotation = exampleClass.getAnnotation(CustomTypeAnnotation.class);
            CustomTypeAnnotation customTypeAnnotation = (CustomTypeAnnotation) annotation;

            System.out.printf("%nPriority :%s", customTypeAnnotation.priority());
            System.out.printf("%nCreate by :%s", customTypeAnnotation.createBy());
            System.out.printf("%nTags :%s", (Object) customTypeAnnotation.tags());

            int tagLength = customTypeAnnotation.tags().length;
            for (String tag: customTypeAnnotation.tags()){
                if (tagLength > 1){
                    System.out.println(tag + ", ");
                }else {
                    System.out.println(tag);

                }
                tagLength--;

            }
            for (Method method : exampleClass.getDeclaredMethods()){
                if (method.isAnnotationPresent(CustomMethodAnnotaation.class)){
                    Annotation annotation1 = method.getAnnotation(CustomMethodAnnotaation.class);
                    CustomMethodAnnotaation customMethodAnnotaation = (CustomMethodAnnotaation) annotation1;
                    if (customMethodAnnotaation.isenable()){
                        String result = "n/a";
                        try {
                            result = (String) method.invoke(exampleClass.newInstance());
                            System.out.printf("%s - customMethod  '%s' - processed %n-result: %n",
                                    ++total,
                                    method.getName(),
                                    result);
                            success++;
                        }catch (Throwable exception){
                            System.out.printf("%s - customMethod '%s' - didn't process:  %s %n",
                                    ++total,
                                    method.getName(),
                                    exception.getCause());
                            failed++;
                        }

                    }else {
                        System.out.printf("%s - customMethod '%s' - didn't process%n",
                                ++total,
                                method.getName());
                        disabled++;

                    }

                }
            }
            System.out.printf("%nResult: Total: %d, Successful: %d, Failed %d, Disabled %d%n",
                    total,
                    success,
                    failed,
                    disabled);

        }
    }
}
