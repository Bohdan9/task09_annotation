package models;

import annotations.CustomMethodAnnotaation;
import annotations.CustomTypeAnnotation;

@CustomTypeAnnotation(
        priority = CustomTypeAnnotation.Priority.HIGH,
        createBy = "Artik",
        tags = {"java", "annotations"}
)
public class AnnotationExample {

    @CustomMethodAnnotaation
    String shouldAlwaysProcessed(){
        return "This should always be processed";
    }
    @CustomMethodAnnotaation
    void willThrowExeption(){
        throw new RuntimeException("This will throw an exception");


    }
    @CustomMethodAnnotaation(isenable = false)
    void shouldNotBeProcessed(){
        throw new RuntimeException("This should never be processed");

    }

}
